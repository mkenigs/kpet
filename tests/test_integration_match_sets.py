# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration tests expecting a match"""
from tests.test_integration import (IntegrationTests, kpet_run_generate,
                                    COMMONTREE_XML, create_asset_files)


class IntegrationMatchSetsTests(IntegrationTests):
    """Integration tests expecting a match in cases"""

    def test_match_abstract_case(self):
        """Check abstract case's subcases are included"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree:
                        template: tree.xml
                sets:
                    foo: "Lorem"
                    bar: "ipsum"
                    baz: "dolor"
                case:
                  host_type_regex: ^normal
                  location: somewhere
                  max_duration_seconds: 600
                  cases:
                    A:
                      name: A
                      maintainers:
                        - maint1
                      sets: foo
                      cases:
                        a:
                          name: a
                    B:
                      name: B
                      maintainers:
                        - maint2
                      sets: bar
                      cases:
                        b:
                          name: b
                """,
            "tree.xml": COMMONTREE_XML,
        }

        assets_path = create_asset_files(self.test_dir, assets)

        # Matches an abstract case
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "-s", "foo",
            stdout_matching=r'.*<job>\s*HOST\s*A - a\s*</job>.*')
        # Matches another abstract case
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "-s", "bar",
            stdout_matching=r'.*<job>\s*HOST\s*B - b\s*</job>.*')
        # Doesn't match any cases
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "-s", "baz",
            stdout_matching=r'.*<job>\s</job>.*')

    def test_match_test_case(self):
        """Check a test case is included when matching"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree:
                        template: tree.xml
                sets:
                    foo: "Lorem"
                    bar: "ipsum"
                    baz: "dolor"
                case:
                  host_type_regex: ^normal
                  location: somewhere
                  max_duration_seconds: 600
                  cases:
                    A:
                      name: A
                      maintainers:
                        - maint1
                      sets: foo
                    B:
                      name: B
                      maintainers:
                        - maint2
                      sets: bar
                """,
            "tree.xml": COMMONTREE_XML,
        }

        assets_path = create_asset_files(self.test_dir, assets)

        # Matches a test case
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "-s", "foo",
            stdout_matching=r'.*<job>\s*HOST\s*A\s*</job>.*')
        # Matches a different test case
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "-s", "bar",
            stdout_matching=r'.*<job>\s*HOST\s*B\s*</job>.*')

    def test_match_not_subset_error(self):
        """
        Check we detect a test case with sets its super-case doesn't have
        """
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree:
                        template: tree.xml
                sets:
                    foo: "Lorem"
                    bar: "ipsum"
                    baz: "dolor"
                case:
                  host_type_regex: ^normal
                  cases:
                    A:
                      name: A
                      location: somewhere
                      maintainers:
                        - maint1
                      sets:
                        - foo
                        - bar
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
                          sets:
                            - foo
                            - baz
                """,
            "tree.xml": COMMONTREE_XML,
        }

        assets_path = create_asset_files(self.test_dir, assets)
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "-s", "foo", status=1,
            stderr_matching=r".* doesn't match any of the available sets: .*")

    def test_match_nonexistent_set_test_case_error(self):
        """
        Check we get an error when specifying an unknown set in a test case
        """
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree:
                        template: tree.xml
                sets:
                    foo: "Lorem"
                    bar: "ipsum"
                    baz: "dolor"
                case:
                  host_type_regex: ^normal
                  cases:
                    A:
                      name: A
                      location: somewhere
                      maintainers:
                        - maint1
                      sets:
                        - foo
                        - bar
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
                          sets:
                            - foo
                            - baz
                            - unknown
                """,
            "tree.xml": COMMONTREE_XML,
        }

        assets_path = create_asset_files(self.test_dir, assets)
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "-s", "foo", status=1,
            stderr_matching=r".* doesn't match any of the available sets: .*")

    def test_match_nonexistent_set_abstract_case_error(self):
        """
        Check we get an error when specifying an unknown set in abstract case
        """
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree:
                        template: tree.xml
                sets:
                    foo: "Lorem"
                    bar: "ipsum"
                    baz: "dolor"
                case:
                  host_type_regex: ^normal
                  cases:
                    A:
                      name: A
                      location: somewhere
                      maintainers:
                        - maint1
                      sets:
                        - foo
                        - bar
                        - unknown
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
                          sets:
                            - foo
                            - baz
                """,
            "tree.xml": COMMONTREE_XML,
        }

        assets_path = create_asset_files(self.test_dir, assets)
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "-s", "foo", status=1,
            stderr_matching=r".* doesn't match any of the available sets: .*")

    def test_match_nonexistent_set_error(self):
        """
        Make sure we get an error when specifying
        an unknown set in command line arguments
        """
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree:
                        template: tree.xml
                sets:
                    foo: "Lorem"
                    bar: "ipsum"
                    baz: "dolor"
                case:
                  host_type_regex: ^normal
                  cases:
                    A:
                      name: A
                      location: somewhere
                      maintainers:
                        - maint1
                      sets:
                        - foo
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
                          sets:
                            - foo
                """,
            "tree.xml": COMMONTREE_XML,
        }

        assets_path = create_asset_files(self.test_dir, assets)
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "-s", "unknown", status=1,
            stderr_matching=r'.*No test sets matched specified regular.*')

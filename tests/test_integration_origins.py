# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Suite origins integration tests"""
from tests.test_integration import (IntegrationTests, kpet_run_generate,
                                    create_asset_files)


# index.yaml without origins defined
INDEX_YAML = """
                    host_types:
                        normal: {}
                    recipesets:
                        rcs1:
                          - normal
                    arches:
                        - arch
                    trees:
                        tree:
                            template: tree.xml
                    case:
                        host_type_regex: normal
                        max_duration_seconds: 100
                        maintainers: [maintainer]
"""

# index.yaml with two origins ("X" and "Y") defined
INDEX_YAML_WITH_ORIGINS = """
                    origins:
                        X: X locations
                        Y: Y locations
""" + INDEX_YAML


class IntegrationOriginsTests(IntegrationTests):
    """Integration tests for test origins"""

    def test_db_without_origins_and_test_without_origin_works(self):
        """Check no origins at all works"""
        assets_path = create_asset_files(
            self.test_dir,
            {
                "index.yaml": INDEX_YAML + """
                        location: somewhere
                """,
                "tree.xml": ""
            }
        )
        self.assertKpetProduces(kpet_run_generate, assets_path,
                                "--no-lint", stdout_matching="")

    def test_db_without_origins_and_test_with_origin_fails(self):
        """Check using test origins without origins defined fails"""
        assets_path = create_asset_files(
            self.test_dir,
            {
                "index.yaml": INDEX_YAML + """
                        origin: X
                        location: somewhere
                """,
                "tree.xml": ""
            }
        )
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "--no-lint", status=1,
            stderr_matching=r'.* has origin specified\b.*')

    def test_db_with_origins_and_test_without_origin_fails(self):
        """Check not using test origins with origins defined fails"""
        assets_path = create_asset_files(
            self.test_dir,
            {
                "index.yaml": INDEX_YAML_WITH_ORIGINS + """
                        location: somewhere
                """,
                "tree.xml": ""
            }
        )
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "--no-lint", status=1,
            stderr_matching=r'.* has no origin specified\b.*')

    def test_db_with_origins_and_test_with_origin_x_works(self):
        """Check using one of two defined origins works"""
        assets_path = create_asset_files(
            self.test_dir,
            {
                "index.yaml": INDEX_YAML_WITH_ORIGINS + """
                        origin: X
                        location: somewhere
                """,
                "tree.xml": ""
            }
        )
        self.assertKpetProduces(kpet_run_generate, assets_path,
                                "--no-lint", stdout_matching="")

    def test_db_with_origins_and_test_with_origin_y_works(self):
        """Check using the other one of two defined origins works"""
        assets_path = create_asset_files(
            self.test_dir,
            {
                "index.yaml": INDEX_YAML_WITH_ORIGINS + """
                        origin: Y
                        location: somewhere
                """,
                "tree.xml": ""
            }
        )
        self.assertKpetProduces(kpet_run_generate, assets_path,
                                "--no-lint", stdout_matching="")

    def test_db_with_origins_and_test_with_unknown_origin_fails(self):
        """Check using unknown origin fails"""
        assets_path = create_asset_files(
            self.test_dir,
            {
                "index.yaml": INDEX_YAML_WITH_ORIGINS + """
                        origin: Z
                        location: somewhere
                """,
                "tree.xml": ""
            }
        )
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "--no-lint", status=1,
            stderr_matching=r'.* has unknown origin specified: "Z".*')
